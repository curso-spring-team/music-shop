package pe.com.tutoriales.musicshop.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pe.com.tutoriales.musicshop.model.Sucursal;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class SucursalDAOTest {
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	private SucursalDAO sucursalDAO;
	
	@Before
    public void setUp() {
   
		namedParameterJdbcTemplate.update(
				"DELETE SUCURSAL",
				new MapSqlParameterSource());
	}
	
	

	@Test
	public void insertarSucursalCorrectamente() {
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("01");
		sucursal.setNombre("Lima");
		
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);
		
		assertEquals("Lima",sucursalInsertada.getNombre());
		assertEquals("01",sucursalInsertada.getCodigo());
		assertNotNull(sucursalInsertada.getIdSucursal());
	}
	
	@Test(expected=Exception.class)
	public void errorAlInsertarSucusalSinCodigo() {
		Sucursal sucursal=new Sucursal();
		sucursal.setNombre("Lima");
		
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);
			
	}
	
	@Test(expected=Exception.class)
	public void errorAlInsertarSucusalSinNombre() {
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("01");
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);;
	}
	
	@Test
	public void obtenerPorIdSucursalCorrectamente() {
		
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("02");
		sucursal.setNombre("Lima2");
		
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);

		Sucursal sucursalEncontrada=sucursalDAO.obtenerPorIdSucursal(sucursalInsertada);
		
		assertEquals("Lima2",sucursalInsertada.getNombre());
		assertEquals("02",sucursalInsertada.getCodigo());
		assertEquals(sucursalInsertada.getIdSucursal(),sucursalEncontrada.getIdSucursal());
		
	}
	
	@Test
	public void actualizarSucursalCorrectamente() {
		
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("03");
		sucursal.setNombre("Lima3");
		
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);
		
		sucursalInsertada.setCodigo("03Modificado");
		sucursalInsertada.setNombre("Lima3Modificado");
		
		sucursalDAO.actualizar(sucursalInsertada);

		Sucursal sucursalActualizada=sucursalDAO.obtenerPorIdSucursal(sucursalInsertada);
		
		assertEquals("03Modificado",sucursalActualizada.getCodigo());
		assertEquals("Lima3Modificado",sucursalActualizada.getNombre());
		assertEquals(sucursalInsertada.getIdSucursal(),sucursalActualizada.getIdSucursal());
		
	}
	
	@Test
	public void eliminarSucursalCorrectamente() {
		
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("03");
		sucursal.setNombre("Lima3");
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);

		assertNotNull(sucursalInsertada);
		sucursalDAO.eliminar(sucursalInsertada);
		Sucursal sucursalEncontrada=sucursalDAO.obtenerPorIdSucursal(sucursalInsertada);
		
		assertNull(sucursalEncontrada);
	}
	
	@Test
	public void buscarSucursalCorrectamente() {
		
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("0369");
		sucursal.setNombre("CERCADO LIMA");
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);
		
		Sucursal sucursalABuscar=new Sucursal();
		sucursalABuscar.setNombre("CERCADO L");
		
		List<Sucursal> listaSucursalesEncontradas=sucursalDAO.buscar(sucursalABuscar);
		
		assertEquals("0369",listaSucursalesEncontradas.get(0).getCodigo());
		assertEquals("CERCADO LIMA",listaSucursalesEncontradas.get(0).getNombre());
		assertEquals(sucursalInsertada.getIdSucursal(),listaSucursalesEncontradas.get(0).getIdSucursal());

	}
	
	@Test
	public void listarSucursalesPorCodigoCorrectamente() {
		
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("0369");
		sucursal.setNombre("CERCADO LIMA");
		Sucursal sucursalInsertada=sucursalDAO.insertar(sucursal);
		
		Sucursal sucursalABuscar=new Sucursal();
		sucursalABuscar.setCodigo("0369");
		
		List<Sucursal> listaSucursalesEncontradas=sucursalDAO.listarSucursalesPorCodigo(sucursalABuscar);
		
		assertEquals("0369",listaSucursalesEncontradas.get(0).getCodigo());
		assertEquals("CERCADO LIMA",listaSucursalesEncontradas.get(0).getNombre());
		assertEquals(sucursalInsertada.getIdSucursal(),listaSucursalesEncontradas.get(0).getIdSucursal());

	}

}
