package pe.com.tutoriales.musicshop.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pe.com.tutoriales.musicshop.exeptions.BusinessException;
import pe.com.tutoriales.musicshop.model.Sucursal;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class SucursalServiceTest {
	@Autowired
	private SucursalService sucursalService;
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	/*@Before
    public void setUp() {
   
		namedParameterJdbcTemplate.update(
				"DELETE SUCURSAL",
				new MapSqlParameterSource());
	}

	@Test
	public void insertarSurursalCorrectamente() throws BusinessException {
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("01");
		sucursal.setNombre("Lima");
		
		Sucursal sucursalInsertada=sucursalService.registrarSucursal(sucursal);
		
		assertEquals("Lima",sucursalInsertada.getNombre());
		assertEquals("01",sucursalInsertada.getCodigo());
		assertNotNull(sucursalInsertada.getIdSucursal());
	}
	
	@Test(expected=BusinessException.class)
	public void errorAlregistrarSucursalConCodigoYaExistente() throws BusinessException {
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("01");
		sucursal.setNombre("Lima");
		
		sucursalService.registrarSucursal(sucursal);
		
		Sucursal sucursal2=new Sucursal();
		sucursal2.setCodigo("01");
		sucursal2.setNombre("MIRAFLORES");
		
		Sucursal sucursalInsertada=sucursalService.registrarSucursal(sucursal2);
		
	}
	
	@Test
	public void errorAlregistrarSucursalConCodigoYaExistenteVerificandoMensaje() throws BusinessException {
		
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("01");
		sucursal.setNombre("Lima");
		
		sucursalService.registrarSucursal(sucursal);
		
		Sucursal sucursal2=new Sucursal();
		sucursal2.setCodigo("01");
		sucursal2.setNombre("MIRAFLORES");
		try {
			Sucursal sucursalInsertada=sucursalService.registrarSucursal(sucursal2);
		} catch (BusinessException e) {
			assertEquals("Ya existe una sucursal con el mismo codigo", e.getMessage());
		}
		
		
	}*/

	/*@Test
	public void rollbackAlOcurrirUnaExepcion() throws BusinessException {
		
		Sucursal sucursal=new Sucursal();
		sucursal.setCodigo("555555");
		sucursal.setNombre("sucu");
		
		sucursalService.dosOperaciones(sucursal);
		
		
	}*/
	
	
}
