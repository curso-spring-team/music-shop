package pe.com.tutoriales.musicshop.dao;

import java.util.List;

import pe.com.tutoriales.musicshop.model.Sucursal;

public interface SucursalDAO {

	Sucursal insertar(Sucursal sucursal);

	Sucursal obtenerPorIdSucursal(Sucursal sucursal);

	void actualizar(Sucursal sucursal);

	void eliminar(Sucursal sucursal);

	List<Sucursal> buscar(Sucursal sucursal,Integer pagina,Integer registros);
	Integer buscarTotal(Sucursal sucursal);

	List<Sucursal> listarSucursalesPorCodigo(Sucursal sucursal);

}
