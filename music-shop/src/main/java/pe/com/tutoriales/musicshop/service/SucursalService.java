package pe.com.tutoriales.musicshop.service;

import java.util.List;

import pe.com.tutoriales.musicshop.exeptions.BusinessException;
import pe.com.tutoriales.musicshop.model.Sucursal;

public interface SucursalService {

	Sucursal registrarSucursal(Sucursal sucursal) throws BusinessException;
	
	void actualizarSucursal(Sucursal sucursal) throws BusinessException;
	void eliminarSucursal(Sucursal sucursal) throws BusinessException;
	List<Sucursal> buscarSucursal(Sucursal sucursal,Integer pagina,Integer registros) throws BusinessException;
	Integer buscarSucursalTotal(Sucursal sucursal) throws BusinessException;
	Sucursal obtenerSucursalPorId(Sucursal sucursal) throws BusinessException;
}
