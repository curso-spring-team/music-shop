package pe.com.tutoriales.musicshop.model;

public class Sucursal {
	private Integer idSucursal;
	private String nombre;
	private String codigo;
	public Sucursal() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Sucursal(Integer idSucursal, String nombre) {
		super();
		this.idSucursal = idSucursal;
		this.nombre = nombre;
	}
	public Integer getIdSucursal() {
		return idSucursal;
	}
	public void setIdSucursal(Integer idSucursal) {
		this.idSucursal = idSucursal;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Sucursal [idSucursal=");
		builder.append(idSucursal);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", codigo=");
		builder.append(codigo);
		builder.append("]");
		return builder.toString();
	}
		
	
	
}
