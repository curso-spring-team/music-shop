package pe.com.tutoriales.musicshop.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import pe.com.tutoriales.musicshop.dao.SucursalDAO;
import pe.com.tutoriales.musicshop.model.Sucursal;

@Repository
public class SucursalDAOImpl implements SucursalDAO {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Sucursal insertar(Sucursal sucursal) {

		sucursal.setIdSucursal(namedParameterJdbcTemplate.queryForObject(
				"SELECT SEC_SUCURSAL.nextval  FROM DUAL",
				new MapSqlParameterSource(), Integer.class));

		SqlParameterSource sucursalParametros = new BeanPropertySqlParameterSource(
				sucursal);

		namedParameterJdbcTemplate
				.update("INSERT INTO SUCURSAL(ID_SUCURSAL, NOMBRE, CODIGO) VALUES(:idSucursal, :nombre, :codigo)",
						sucursalParametros);
		return sucursal;
	}

	public Sucursal obtenerPorIdSucursal(Sucursal sucursal) {

		SqlParameterSource sucursalParametros = new BeanPropertySqlParameterSource(
				sucursal);
		Sucursal sucursalEncontrada;
		List<Sucursal> listaSucursales;

		listaSucursales = namedParameterJdbcTemplate.query(
				"SELECT * FROM SUCURSAL WHERE ID_SUCURSAL=:idSucursal",
				sucursalParametros, new BeanPropertyRowMapper(Sucursal.class));

		if (listaSucursales.size() > 0) {
			return listaSucursales.get(0);
		} else {
			return null;
		}
	}

	public void actualizar(Sucursal sucursal) {

		SqlParameterSource sucursalParametros = new BeanPropertySqlParameterSource(
				sucursal);

		namedParameterJdbcTemplate
				.update("UPDATE SUCURSAL SET CODIGO=:codigo , NOMBRE=:nombre WHERE ID_SUCURSAL=:idSucursal",
						sucursalParametros);

	}

	public void eliminar(Sucursal sucursal) {

		SqlParameterSource sucursalParametros = new BeanPropertySqlParameterSource(
				sucursal);

		namedParameterJdbcTemplate.update(
				"DELETE SUCURSAL WHERE ID_SUCURSAL=:idSucursal",
				sucursalParametros);

	}

	public List<Sucursal> buscar(Sucursal sucursal, Integer pagina,
			Integer registros) {
		
		Integer inicio = (pagina - 1) * registros + 1;
		Integer fin = inicio + registros - 1;

		if (sucursal.getCodigo() == null)
			sucursal.setCodigo("");
		if (sucursal.getNombre() == null)
			sucursal.setNombre("");

		sucursal.setCodigo("%" + sucursal.getCodigo() + "%");
		sucursal.setNombre("%" + sucursal.getNombre() + "%");
		SqlParameterSource sucursalParametros = new BeanPropertySqlParameterSource(
				sucursal);
		
		Sucursal sucursalEncontrada;
		List<Sucursal> listaSucursales;

		/* "SELECT ID_SUCURSAL as idSucursal, CODIGO as codigo,NOMBRE as nombre, ROW_NUMBER() OVER (ORDER BY nombre) R FROM SUCURSAL WHERE NOMBRE LIKE :nombre AND CODIGO LIKE :codigo" */
		listaSucursales = namedParameterJdbcTemplate
				.query("SELECT * FROM (SELECT ID_SUCURSAL, CODIGO, NOMBRE, ROW_NUMBER() OVER (ORDER BY NOMBRE) R FROM SUCURSAL WHERE NOMBRE LIKE :nombre AND CODIGO LIKE :codigo) WHERE R BETWEEN "+inicio+" AND "+fin
				, sucursalParametros, new BeanPropertyRowMapper(Sucursal.class));

		return listaSucursales;
	}

	public Integer buscarTotal(Sucursal sucursal) {

		if (sucursal.getCodigo() == null)
			sucursal.setCodigo("");
		if (sucursal.getNombre() == null)
			sucursal.setNombre("");

		sucursal.setCodigo("%" + sucursal.getCodigo() + "%");
		sucursal.setNombre("%" + sucursal.getNombre() + "%");
		SqlParameterSource sucursalParametros = new BeanPropertySqlParameterSource(
				sucursal);

		Sucursal sucursalEncontrada;
		List<Sucursal> listaSucursales;

		listaSucursales = namedParameterJdbcTemplate
				.query("SELECT * FROM SUCURSAL WHERE NOMBRE LIKE :nombre AND CODIGO LIKE :codigo",
						sucursalParametros, new BeanPropertyRowMapper(
								Sucursal.class));

		return listaSucursales.size();
	}

	public List<Sucursal> listarSucursalesPorCodigo(Sucursal sucursal) {

		SqlParameterSource sucursalParametros = new BeanPropertySqlParameterSource(
				sucursal);

		Sucursal sucursalEncontrada;
		List<Sucursal> listaSucursales;

		listaSucursales = namedParameterJdbcTemplate.query(
				"SELECT * FROM SUCURSAL WHERE  CODIGO =:codigo",
				sucursalParametros, new BeanPropertyRowMapper(Sucursal.class));

		return listaSucursales;
	}

}
