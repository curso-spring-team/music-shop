package pe.com.tutoriales.musicshop.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.tutoriales.musicshop.dao.SucursalDAO;
import pe.com.tutoriales.musicshop.exeptions.BusinessException;
import pe.com.tutoriales.musicshop.model.Sucursal;
import pe.com.tutoriales.musicshop.service.SucursalService;

@Service
@Transactional(rollbackFor=Exception.class)
public class SucursalServiceImpl implements SucursalService {

	@Autowired
	private SucursalDAO sucursalDAO;

	public Sucursal registrarSucursal(Sucursal sucursal) throws BusinessException {
		
		if(sucursalDAO.listarSucursalesPorCodigo(sucursal).size()>0){
			throw new BusinessException("Ya existe una sucursal con el mismo codigo");
		}else{
			return sucursalDAO.insertar(sucursal);
		}
	}

	public void actualizarSucursal(Sucursal sucursal) throws BusinessException {
		List<Sucursal> listaSucursales=sucursalDAO.listarSucursalesPorCodigo(sucursal);
		if(listaSucursales.size()==0){
			sucursalDAO.actualizar(sucursal);
		}else if (sucursalDAO.listarSucursalesPorCodigo(sucursal).size()==1){
			sucursalDAO.actualizar(sucursal);
		}else {
			throw new BusinessException("Ya existe una sucursal con el mismo codigo");
		}
		
	}

	public void eliminarSucursal(Sucursal sucursal) throws BusinessException {
		this.sucursalDAO.eliminar(sucursal);
		
	}

	public List<Sucursal> buscarSucursal(Sucursal sucursal,Integer pagina,Integer registros)
			throws BusinessException {
		return this.sucursalDAO.buscar(sucursal,pagina,registros);
	}
	public Integer buscarSucursalTotal(Sucursal sucursal)
			throws BusinessException {
		return this.sucursalDAO.buscarTotal(sucursal);
	}

	public Sucursal obtenerSucursalPorId(Sucursal sucursal)
			throws BusinessException {
		return this.sucursalDAO.obtenerPorIdSucursal(sucursal);
	}


	
	

}
