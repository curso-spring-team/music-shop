package pe.com.tutoriales.musicshop.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import pe.com.tutoriales.musicshop.beans.ResponseListBean;
import pe.com.tutoriales.musicshop.exeptions.BusinessException;
import pe.com.tutoriales.musicshop.model.Sucursal;
import pe.com.tutoriales.musicshop.service.SucursalService;
import pe.com.tutoriales.musicshop.util.OperadoresUtil;

@Controller
public class GestionDeSucursalesController extends ExceptionHandlingController {

	@Autowired
	private SucursalService sucursalService;

	@RequestMapping("gestion-de-sucursales.htm")
	public String irAGestionDeSucursales() {

		return "gestion-de-sucursales";
	}

	@RequestMapping(value = "/v1/sucursales", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.ALL_VALUE)
	public @ResponseBody ResponseListBean<Sucursal> getSucursal(
			Sucursal sucursal,
			@RequestParam(value = "page", defaultValue = "1") Integer pagina,
			@RequestParam(value = "rows", defaultValue = "10") Integer registros,
			Boolean search) throws BusinessException {
		
		System.out.println("pagina:"+pagina);
		System.out.println("registros:"+registros);
		
		ResponseListBean<Sucursal> response=new ResponseListBean<Sucursal>();
		
		if (search) {
			
			Integer totalRegistros =this.sucursalService.buscarSucursalTotal(sucursal);

			response.setPage(pagina);
			response.setRecords(totalRegistros);
			response.setTotal(OperadoresUtil.obtenerCociente(totalRegistros,
					registros));
			response.setRows(this.sucursalService.buscarSucursal(sucursal,pagina,registros));


			return response;
		} else {
			return response;
		}

	}

	@RequestMapping(value = "/v1/sucursales", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Sucursal registrarSucursal(
			@RequestBody Sucursal sucursal) throws BusinessException {
		System.out.println(sucursal);
		return this.sucursalService.registrarSucursal(sucursal);

	}

	@RequestMapping(value = "/v1/sucursales", method = RequestMethod.PUT, produces = "application/json")
	public @ResponseBody Sucursal actualizarSucursal(
			@RequestBody Sucursal sucursal) throws BusinessException {

		System.out.println(sucursal);
		this.sucursalService.actualizarSucursal(sucursal);
		return sucursal;
	}

	@RequestMapping(value = "/v1/sucursales", method = RequestMethod.DELETE, produces = "application/json")
	public @ResponseBody Sucursal eliminarSucursal(
			@RequestBody Sucursal sucursal) throws BusinessException {
		this.sucursalService.eliminarSucursal(sucursal);
		return sucursal;
	}

	@RequestMapping(value = "/v1/sucursales/{idSucursal}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Sucursal obtenerSucursalPorId(
			@PathVariable Integer idSucursal) throws BusinessException {
		Sucursal sucursal = new Sucursal();
		sucursal.setIdSucursal(idSucursal);
		return this.sucursalService.obtenerSucursalPorId(sucursal);

	}

}
