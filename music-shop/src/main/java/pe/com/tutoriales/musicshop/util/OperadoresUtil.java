package pe.com.tutoriales.musicshop.util;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class OperadoresUtil {

	public static final String CONTEXT = "context";
	public static final String VACIO = "";
	public static final String COMA = ",";
	public static final String ESPACIO = " ";
	public static final String FORMATO_FECHA_HORA = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMATO_FECHA = "dd/MM/yyyy";
	public static final String FORMATO_HORA = "HH:mm:ss";
	public static final String SI = "si";
	public static final String NO = "no";

	protected OperadoresUtil() {
	}

	public static String dateToString(Date fecha) {

		SimpleDateFormat formato = new SimpleDateFormat(FORMATO_FECHA);
		return formato.format(fecha);
	}

	public static Integer obtenerCociente(Integer dividendo, Integer divisor) {
		if ((dividendo % divisor) == 0) {
			return dividendo / divisor;
		} else {
			return dividendo / divisor + 1;
		}

	}

	public static Date stringToDate(String fecha) {

		DateFormat horaFormato = new SimpleDateFormat(FORMATO_HORA);
		Date horaActual = new Date();

		String fechaCompleta = fecha + ESPACIO + horaFormato.format(horaActual);

		DateFormat fechaFormato;
		Date dateActual = null;
		fechaFormato = new SimpleDateFormat(FORMATO_FECHA_HORA);

		if (fechaCompleta == null || fechaCompleta.equals(VACIO)) {
			return null;
		} else {
			try {
				dateActual = fechaFormato.parse(fechaCompleta);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return dateActual;
		}

	}

	public static Date stringToDateSinHora(String fecha) {

		DateFormat fechaFormato;
		Date dateActual = null;
		fechaFormato = new SimpleDateFormat(FORMATO_FECHA);

		if (fecha == null || fecha.equals(VACIO)) {
			return null;
		} else {
			try {
				dateActual = fechaFormato.parse(fecha);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			return dateActual;
		}

	}

	public static Double stringToDouble(String valor) {

		Double resultado = 0.0;

		if (valor.equals(VACIO) || valor == null) {
			resultado = 0.0;
		}

		if (!valor.equals(VACIO)) {
			String valorRemplazado = valor.replace(COMA, VACIO);
			resultado = Double.parseDouble(valorRemplazado);
		}

		return resultado;
	}

	public static Integer stringToInteger(String valor) {

		if (!valor.equals(VACIO)) {
			return Integer.parseInt(valor);
		}
		return 0;
	}

	public static boolean integerToBoolean(Integer valor) {

		boolean resultado = false;

		if (valor == 1) {
			resultado = true;
		}

		return resultado;

	}

	public static String integerToResponse(Integer valor) {

		String resultado = NO;

		if (valor == 1) {
			resultado = SI;
		}

		return resultado;

	}

	public static int diferenciasDeFechas(Date fechaInicial, Date fechaFinal) {

		Date fechaInicialTemporal = null;
		Date fechaFinalTemporal = null;

		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
		String fechaInicioString = df.format(fechaInicial);

		try {
			fechaInicialTemporal = df.parse(fechaInicioString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String fechaFinalString = df.format(fechaFinal);

		try {
			fechaFinalTemporal = df.parse(fechaFinalString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		long fechaInicialMs = fechaInicialTemporal.getTime();
		long fechaFinalMs = fechaFinalTemporal.getTime();
		long diferencia = fechaFinalMs - fechaInicialMs;
		double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
		return (int) dias;

	}

	public static String obtenerRutaTemporales() {

		File currentDirectory = new File(new File(".").getAbsolutePath());

		String ruta = currentDirectory.getAbsolutePath();
		int indice = ruta.indexOf("bin");
		ruta = ruta.substring(0, indice - 1) + "\\standalone\\tmp\\";

		return ruta;
	}

	public static String obtenerRutaTemporal() {
		File currentDirectory = new File(new File(".").getAbsolutePath());

		String ruta = currentDirectory.getPath();
		int indice = ruta.indexOf("bin");
		ruta = ruta.substring(0, indice - 1);
		ruta = ruta + obtenerSeparadorCarpetas() + "standalone"
				+ obtenerSeparadorCarpetas() + "tmp"
				+ obtenerSeparadorCarpetas();

		return ruta;
	}

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		return os.indexOf("win") >= 0;

	}

	public static String obtenerSeparadorCarpetas() {
		if (isWindows()) {
			return "\\";
		} else {
			return "/";
		}
	}

}
