package pe.com.tutoriales.musicshop.security.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("usuarioDetalleService")
@Transactional(readOnly = true)
public class UsuarioDetalleSeviceImpl implements UserDetailsService,
		Serializable {

	private static final long serialVersionUID = 3214994025111677700L;



	@Override
	public UserDetails loadUserByUsername(String username) {

		User user = obtenerUserPorUsuarioLogin();
		System.out.println("user:"+user);
		if (user == null) {
			
			throw new UsernameNotFoundException("UserAccount for name \""
					+ username + "\" not found.");
			
		}

		return user;
	}

	private User obtenerUserPorUsuarioLogin() {


			User user = new User("admin", "123456",
					true, true, true, true,
					transformarPerfileAListaGrantedAuthority());
			return user;
	}

	private Collection<GrantedAuthority> transformarPerfileAListaGrantedAuthority() {

		Collection<GrantedAuthority> listaGrantedAuthority = new ArrayList<GrantedAuthority>();

			GrantedAuthority grantedAuthorityMenu = new SimpleGrantedAuthority(
					"ADMINISTRADOR");
			listaGrantedAuthority.add(grantedAuthorityMenu);
		return listaGrantedAuthority;
	}

}
