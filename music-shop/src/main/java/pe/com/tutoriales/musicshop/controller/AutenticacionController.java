package pe.com.tutoriales.musicshop.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AutenticacionController {

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	@RequestMapping(value = "/autenticarse.htm", method = RequestMethod.GET)
	public String autenticacionAutomatica(
			@RequestParam(value = "usuario", defaultValue = "admin") String usuario,
			@RequestParam(value = "contrasenia", defaultValue = "123456") String contrasenia,
			HttpServletRequest request) {

		usuario="admin";
		contrasenia="123456";
		System.out.println("token:" + usuario);

		String urlInicial = "";

		boolean esCorrecto=true;
		try {
			Authentication authenticate = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(
							"admin", "123456"));

			if (authenticate.isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(
						authenticate);

				esCorrecto = true;
			}

		} catch (AuthenticationException e) {
			e.printStackTrace();
			esCorrecto = false;
		} catch (Exception e) {
			e.printStackTrace();
			esCorrecto = false;
		}

		if (esCorrecto) {
			urlInicial="gestion-de-sucursales.htm";
		} else {
			urlInicial="login.htm";
		}
		

		return "redirect:" + urlInicial;

	}

	@RequestMapping(value = "/seguridad/acceso-denegado.htm", method = RequestMethod.GET)
	public String accesoDenegado() {

		

		return "cliente/seguridad/acceso-denegado";
	}
	
	@RequestMapping("login.htm")
	public String irALogin() {

		return "publico/login";
	}


	@RequestMapping(value = "/cerrar-session.htm", method = RequestMethod.GET)
	public String cerrarSession() {

		System.out.println("cerrar SEssion");

		SecurityContextHolder.getContext().setAuthentication(null);
		return "redirect:"+"login.htm";
	}
}
