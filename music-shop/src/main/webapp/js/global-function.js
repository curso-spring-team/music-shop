function mostrarMensajeError(mensaje){
	$.jGrowl(mensaje, { header: 'A Header', 'theme':'error', life: 1000 });
}
function mostrarMensajeInfo(mensaje){
	$.jGrowl(mensaje, { header: 'A Header', 'theme':'info', life: 1000 });
}
function mostrarMensajeWarning(mensaje){
	$.jGrowl(mensaje, { header: 'A Header', 'theme':'warning', life: 1000 });
}

jQuery.fn.resetear = function () {
	  $(this).each (function() { this.reset(); });
	}