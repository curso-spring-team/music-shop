function nuevaSucursal() {
	$("#popupNuevaSucursal").dialog();

	event.preventDefault();
};

function guardarSucursal() {
	var parametros = new Object();
	parametros.nombre = $("#nombreNuevaSucursal").val();
	parametros.codigo = $("#codigoNuevaSucursal").val();

	$.ajax({
		type : "POST",
		url : "rest/v1/sucursales",
		data : JSON.stringify(parametros),
		async : true,
		dataType : "json",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(data, status, jqXHR) {
			mostrarMensajeInfo('La sucursal se registro correctamente');
			$("#popupNuevaSucursal").dialog('close');
			buscarSucursal();
			$("#formNuevaSucursal").resetear();
		},

		error : function(jqXHR, textStatus, errorThrown) {
			respuestaJson = jQuery.parseJSON(jqXHR.responseText);
			mostrarMensajeError(respuestaJson.mensaje);
		},
		complete : function(e, xhr, settings) {
			console.log('ENTRO a COMPLETE');
			console.log('e.status:' + e.status);
		}
	});
	
	

	event.preventDefault();
};

function buscarSucursal() {
	var data = new Object();
	data.codigo = $('#codigo').val();
	data.nombre = $('#nombre').val();

	$("#gridSucursales").jqGrid('setGridParam', {
		url : 'rest/v1/sucursales?search=true',
		datatype : "json",
		postData : data,
		page : 1
	}).trigger("reloadGrid");

	event.preventDefault();
}
$(function() {
	$("#nuevo").button();
	$("#buscar").button();
});

$(function() {

	jQuery("#gridSucursales").jqGrid({
		url : 'rest/v1/sucursales?search=true',
		datatype : "json",
		//postData : data,
		colNames : [ 'Id', 'Codigo', 'Nombre', 'Actions'],
		colModel : [ {
			name : 'idSucursal',
			index : 'idSucursal',
			width : 60,
			sorttype : "int",
			editable: true,
			hidden: true
				
		}, {
			name : 'codigo',
			index : 'codigo',
			width : 90,
			editable: true
		}, {
			name : 'nombre',
			index : 'nombre',
			width : 300,
			editable: true
		}, {
			name : 'Actions',
			index : 'Actions',
			width : 100,
			height : 120,
			editable : true,
			formatter : function(cellvalue, options, rowObject,funcionEditar){
				return opciones(cellvalue, options, rowObject,"abrirPopupActualizarSucursal","eliminarSucursal");
			}
		} ],
		rowNum : 20,
		rowList : [ 10, 20, 30 ],
		pager : '#pagerSucursales',
		sortname : 'idSucursal',
		viewrecords : true,
		sortorder : "desc",
		loadonce : false,
		caption : "Sucursales"
	});
});

function opciones( cellvalue, options, rowObject,funcionEditar,funcionEliminar){
	var botonActualizar="";
	var botonEliminar=""
	if(funcionEditar!=null){
		botonActualizar="<span onclick='javascript:"+funcionEditar+"("+rowObject.idSucursal+")' class='ui-icon  ui-icon-pencil '></span>";
	}
	if(funcionEliminar!=null){
		botonEliminar="<span onclick='javascript:"+funcionEliminar+"("+rowObject.idSucursal+")' class='ui-icon ui-icon-trash  '></span>";
	}
	return botonActualizar+botonEliminar;
}


function abrirPopupActualizarSucursal(idSucursal){
	$("#popupEditarSucursal").dialog();

	event.preventDefault();
}



function eliminarSucursal(idSucursal){
	var parametros = new Object();
	parametros.idSucursal = idSucursal;
	

	$.ajax({
		type : "DELETE",
		url : "rest/v1/sucursales",
		data : JSON.stringify(parametros),
		async : true,
		dataType : "json",

		beforeSend : function(xhr) {
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		},
		success : function(data, status, jqXHR) {
			mostrarMensajeInfo('La sucursal se elimino correctamente');
			
			buscarSucursal();
			
		},

		error : function(jqXHR, textStatus, errorThrown) {
			respuestaJson = jQuery.parseJSON(jqXHR.responseText);
			mostrarMensajeError(respuestaJson.mensaje);
		},
		complete : function(e, xhr, settings) {
			console.log('ENTRO a COMPLETE');
			console.log('e.status:' + e.status);
		}
	});

	event.preventDefault();
}