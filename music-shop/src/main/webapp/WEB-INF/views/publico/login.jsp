<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<meta charset="utf-8">
<title>jQuery UI Example Page</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/jquery-ui.min.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/ui.jqgrid.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/jquery.jgrowl.min.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/global-style.css">

<script src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script>
<script src="<%=request.getContextPath()%>/js/grid.locale-es.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.jqGrid.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.jgrowl.min.js"></script>
<script src="<%=request.getContextPath()%>/js/global-function.js"></script>

</head>
<body>
	<form id="formBuscarSucursal" method="get" action="<%=request.getContextPath()%>/autenticarse.htm">
		<input id="usuario"></input> <input id="contrasenia"></input> <input
			type="submit" value="ingresar"></input>
	</form>
</body>



</html>