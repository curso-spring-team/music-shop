<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<meta charset="utf-8">
<title>jQuery UI Example Page</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/jquery-ui.min.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/ui.jqgrid.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/jquery.jgrowl.min.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/css/global-style.css">

<script src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script>
<script src="<%=request.getContextPath()%>/js/grid.locale-es.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.jqGrid.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.jgrowl.min.js"></script>
<script src="<%=request.getContextPath()%>/js/global-function.js"></script>
<script src="<%=request.getContextPath()%>/js/gestion-sucursales/gestion-sucursales.js"></script>
</head>
<body>
	<form id="formBuscarSucursal" action="">
		<h1>Gestion de Sucursales</h1>
		<fieldset>
			<label for="codigo"> Codigo</label> <input id="codigo" name="codigo"
				type="text" /> <label for="nombre"> Nombre</label> <input
				id="nombre" name="nombre" type="text" />
		</fieldset>
		<button id="nuevo" onclick="nuevaSucursal()">Nuevo</button>
		<button id="buscar" onclick="buscarSucursal()">Buscar</button>
		<a href="cerrar-session.htm">cerrar sesion</a>
		
	</form>


	<table id="gridSucursales"></table>
	<div id="pagerSucursales"></div>
</body>


<div id="popupNuevaSucursal" title="Nueva Sucursal"
	style="display: none">
	<form id="formNuevaSucursal" action="">
		<label for="codigoNuevaSucursal"> Codigo</label> <input
			id="codigoNuevaSucursal" name="codigoNuevaSucursal" type="text" /> <br>
		<label for="nombreNuevaSucursal"> Nombre</label><input
			id="nombreNuevaSucursal" name="nombreNuevaSucursal" type="text" />

		<button id="nuevo" onclick="guardarSucursal()">Guardar</button>
	</form>
</div>

<div id="popupEditarSucursal" title="Editar Sucursal"
	style="display: none">
	<form id="formEditarSucursal" action="">
		<label for="codigoEditarSucursal"> Codigo</label> <input
			id="codigoEditarSucursal" name="codigoEditarSucursal" type="text" /> <br>
		<label for="nombreEditarSucursal"> Nombre</label><input
			id="nombreEditarSucursal" name="nombreEditarSucursal" type="text" />

		<button id="guardarActualizar" onclick="actualizarSucursal()">Guardar</button>
	</form>
</div>
</html>